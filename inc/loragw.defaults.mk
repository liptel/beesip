# Defaults for loragw images

BEESIP_PACKAGES +=  openvpn-openssl=y openssl-util=y \
	iftop=y uciprov=y ca-certificates=y \
	zabbix3-agentd=y zaf=y loracol=y

OWRT_CONFIG_SET += UCIPROV_USE_STATIC=y \
	UCIPROV_URI1="https://loragw.cesnet.cz/uci/default/{cn}/{m}.{fde}" \
	UCIPROV_URI2="https://loragw.cesnet.cz/uci/{board}/{cn}/{m}.{fde}" \
	UCIPROV_URI3="https://loragw.cesnet.cz/uci/{cn}/{mac}/{m}.{fde}" \
	UCIPROV_INTERFACE="wan" \
	UCIPROV_TGZ=y \
	UCIPROV_SU=y \
	UCIPROV_RECOVERY=y \
	UCIPROV_OPKG=y \
	UCIPROV_SERVICES=y \
	UCIPROV_SSLDEPLOY=y \
	UCIPROV_TGZ_ONLYFD=y \
	UCIPROV_SU_ONLYFD=y \
	UCIPROV_DELAY_BEFORE_REBOOT="180" \
	UCIPROV_AUTOSTART=y \
	UCIPROV_REPEAT="120" \
	UCIPROV_RECOVERY_FDFLAG="LO0ORAAA" \
	UCIPROV_MGMT=y \
	UCIPROV_MGMT_SETROOTPW=y \
	UCIPROV_REBOOT_AFTER_CFG_APPLY=y \
	UCIPROV_SSLDEPLOY_SUBJECT="{mac}"

	
