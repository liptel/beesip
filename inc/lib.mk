define owrtenv
$$(cd $(OWRT_DIR); $(OWRT_SHELL) env $1)
endef

define mkdirall
	if [ -d $(OWRT_DIR) ]; then \
	  mkdir -p $(STAMP_DIR) $(OSTAMP_DIR) $(DL_DIR) $(LOG_DIR) $(BEESIP_IMG_DIR); \
	fi
endef

define owrt_env
	(cd $(OWRT_DIR); ./scripts/env $1)
endef

define owrt_testconf
	@if [ "$$(wc -l <$(OWRT_DIR)/.config 2>/dev/null)" -lt 50 ]; then $(RM) -f $(SOWRTCFG) $(OWRT_DIR)/.config; $(BMAKE) config-owrt; fi
endef

define BeesipDefaults
 ifeq ($(filter $(1),$(BEESIP_NODEFAULTS) $(BEESIP_DEFAULTS)),)
  -include inc/$(1).defaults$(DEFAULTS.$(1)).mk
  BEESIP_DEFAULTS += $(1)
 endif
endef

define AddDirFiles
 $(shell cd files && find $(1) -type f -a -not -name '*~' |while read line; do echo $$line:$$(echo $$line |cut -d '/' -f 2-); done)
endef

define AddFiles
	for i in $(OWRT_ADDED_FILES); do f=$(PWD)/files/$$(echo $$i|cut -d ':' -f 1); t=$(OWRT_DIR)/files/$$(echo $$i|cut -d ':' -f 2); mkdir -p $$(dirname $$t); $(CP) $$f $$t; done
endef

define AddExternalFiles
	for i in $(OWRT_ADDED_EXTERNAL_FILES); do f=$(PWD)/external-files/$$(echo $$i|cut -d ':' -f 1); t=$(OWRT_DIR)/files/$$(echo $$i|cut -d ':' -f 2); mkdir -p $$(dirname $$t); $(CP) $$f $$t; done
endef

define AddUciprovDefaults
	for i in $(1); do echo set $$i; done
endef

define AddUciprovServices
	for i in $(1); do echo $$i | tr ':' ' '; done
endef

define AddUciprovOpkg
	for i in $(1); do echo $$i; done 
endef

define AddDefaultOpenWrtFeed
  ifeq ($(OWRT_DEFAULT_FEEDS),)
	OWRT_DEFAULT_FEEDS = "$(1)"
  else
	OWRT_DEFAULT_FEEDS += "\n$(1)"
  endif
endef

define AddOpenWrtFeed
  ifeq ($(ADDED_FEEDS),)
	ADDED_FEEDS += "$(1)"
  else
	ADDED_FEEDS += "\n$(1)"
  endif
endef

define AddExternalFilesRepo
	EXTERNAL_FILES_REPO = "$(1)"
endef

define ConvertToVmdk
	( [ "$(3)" = "flat" ] && { qemu-img convert "$(1)" -O vmdk "$(2)" -o adapter_type=lsilogic,subformat=monolithicFlat; return; }; \
	[ "$(3)" = "stream" ] && { $(PWD)/scripts/VMDKstream.py "$(1)" "$(2)"; return; }; \
	[ "$(3)" = "" ] && { qemu-img convert "$(1)" -O vmdk "$(2)"; return; }; );
endef
