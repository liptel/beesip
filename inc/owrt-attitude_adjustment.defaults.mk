
OWRT_GIT=http://git.openwrt.org/12.09/openwrt.git;$(OWRT_FORCE_GIT_REV)

USE_CUSTOM_FEEDS=1

$(eval $(call AddDefaultOpenWrtFeed,src-git packages http://git.openwrt.org/12.09/packages.git;$(OWRT_FORCE_PACKAGES_GIT_REV)))
$(eval $(call AddDefaultOpenWrtFeed,src-svn luci http://svn.luci.subsignal.org/luci/tags/0.11.1/contrib/package/;$(OWRT_FORCE_LUCI_GIT_REV)))

OWRT_CONFIG_UNSET += PACKAGE_asterisk18
