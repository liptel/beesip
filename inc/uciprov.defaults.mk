
ifneq ($(findstring uciprov,$(BEESIP_PACKAGES) $(BEESIP_DEPENDS)),)
 OWRT_CONFIG_SET += \
	UCIPROV_USE_DNS=y \
	UCIPROV_USE_DNSSEC=y \
	UCIPROV_USE_DHCP=y \
	UCIPROV_USE_STATIC=y \
	UCIPROV_URI1="file:///etc/beesip/defaults/{cn}/{m}.{fde}" \
	UCIPROV_URI2="http://{boxid}@beesip-prov.{domain}/{board}/{cn}/{m}.{fde}" \
	UCIPROV_URI3="http://{boxid}@beesip-prov.{domain}/{mac2}/{cn}/{m}.{fde}" \
	UCIPROV_DNS_PREFIX="beesip-prov" \
	UCIPROV_INTERFACE="wan" \
	UCIPROV_TGZ=y \
	UCIPROV_SU=y \
	UCIPROV_RECOVERY=y \
	UCIPROV_OPKG=y \
	UCIPROV_SERVICES=y \
	UCIPROV_SSLDEPLOY=y \
	UCIPROV_TGZ_ONLYFD=y \
	UCIPROV_SU_ONLYFD=y \
	UCIPROV_REBOOT_AFTER_CFG_APPLY=y \
	UCIPROV_DELAY_BEFORE_REBOOT="180" \
	UCIPROV_AUTOSTART=y \
	UCIPROV_REPEAT="120" \
	UCIPROV_RECOVERY_FDFLAG="beesip" \
	UCIPROV_MGMT=y \
	UCIPROV_MGMT_SETROOTPW=y \
	UCIPROV_SSLDEPLOY_FETCHURI="http://{boxid}@beesip-prov.{domain}/{mac2}/{m}/crt" \
	UCIPROV_SSLDEPLOY_PUSHURI="http://{boxid}@beesip-prov.{domain}/{mac2}/{m}/csr" 

 UCIPROV_UCIDEFAULTS += system.@system[0].hostname=Beesip
 UCIPROV_DEVDEFAULTS += system.@system[0].hostname=Beesip-dev uciprov.opkg.only_on_fd=0 uciprov.global.reboot_delay=1 uciprov.global.debug=3 uciprov.global.uri_static2="" uciprov.global.uri_static3=""
 UCIPROV_SERVICES += dropbear:enable lldpd:disable 
 UCIPROV_DEVSERVICES += dropbear:enable lldpd:disable fstab:enable
 UCIPROV_OPKG +=  "update" "install $(ASTPKG)" "install $(KAMPKG)" "install beesip" "install uciprov"
 UCIPROV_DEVOPKG +=  "add_uri src beesip_local file:///mnt/pkg/beesip" "update" "install $(ASTPKG)" "install $(KAMPKG)" "install beesip" "install uciprov"
endif
