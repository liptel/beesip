BEESIP_NODEFAULTS=generic files callmon sbc pbx

BEESIP_PACKAGES+=\
	iperf=y iperf3=y kmod-netem=y \
	openssh-client-utils=y openssh-keygen=y	openssh-server=y openssh-client=y rsync=y \
	ip-full=y vim=y nano=y mc=y python-pip=y uciprov=y curl=y tcpdump=y sipgrep=y sipcapture=y \
	asterisk15=y asterisk15-app-alarmreceiver=y asterisk15-app-authenticate=y \
	asterisk15-app-chanisavail=y asterisk15-app-chanspy=y asterisk15-app-confbridge=y \
	asterisk15-app-directed_pickup=y asterisk15-app-disa=y asterisk15-app-exec=y \
	asterisk15-app-minivm=y asterisk15-app-mixmonitor=y asterisk15-app-originate=y \
	asterisk15-app-playtones=y asterisk15-app-read=y asterisk15-app-readexten=y \
	asterisk15-app-record=y asterisk15-app-sayunixtime=y asterisk15-app-senddtmf=y \
	asterisk15-app-sms=y asterisk15-app-stack=y asterisk15-app-system=y \
	asterisk15-app-talkdetect=y asterisk15-app-verbose=y asterisk15-app-waituntil=y \
	asterisk15-app-while=y asterisk15-chan-iax2=y asterisk15-chan-sip=y asterisk15-pjsip=y \
	asterisk15-res-hep=y asterisk15-res-hep-pjsip=y asterisk15-res-hep-rtcp=y \
	asterisk15-res-pjproject=y asterisk15-res-realtime=y \
	asterisk15-codec-a-mu=y \
	asterisk15-codec-adpcm=y asterisk15-codec-alaw=y asterisk15-codec-g722=y \
	asterisk15-codec-g726=y asterisk15-codec-gsm=y \
	asterisk15-codec-ilbc=y asterisk15-codec-lpc10=y asterisk15-codec-resample=y \
	asterisk15-curl=y asterisk15-format-g726=y asterisk15-format-g729=y \
	asterisk15-format-gsm=y asterisk15-format-h263=y asterisk15-format-h264=y \
	asterisk15-format-ilbc=y asterisk15-format-sln=y asterisk15-format-vox=y \
	asterisk15-format-wav=y asterisk15-format-wav-gsm=y asterisk15-func-base64=y \
	asterisk15-func-blacklist=y asterisk15-func-channel=y asterisk15-func-cut=y \
	asterisk15-func-db=y asterisk15-func-devstate=y asterisk15-func-enum=y \
	asterisk15-func-env=y asterisk15-func-extstate=y asterisk15-func-global=y \
	asterisk15-func-groupcount=y asterisk15-func-math=y asterisk15-func-module=y \
	asterisk15-func-shell=y asterisk15-func-uri=y asterisk15-func-vmcount=y \
	asterisk15-odbc=y asterisk15-pbx-ael=y asterisk15-pbx-dundi=y \
	asterisk15-pbx-spool=y asterisk15-res-ael-share=y asterisk15-res-agi=y \
	asterisk15-res-clioriginate=y asterisk15-res-hep=y asterisk15-res-hep-rtcp=y asterisk15-res-hep-pjsip=y \
	asterisk15-res-monitor=y asterisk15-res-musiconhold=y asterisk15-res-phoneprov=y \
	asterisk15-res-smdi=y asterisk15-res-srtp=y \
	asterisk15-res-timing-pthread=y asterisk15-res-timing-timerfd=y asterisk15-sounds=y asterisk15-voicemail=y \
        openssl-util=y luci=y wget=y \
        kamailio5=y kamailio5-mod-corex=y kamailio5-mod-auth=y kamailio5-mod-cfg-rpc=y kamailio5-mod-cfgutils=y kamailio5-mod-ctl=y kamailio5-mod-db-sqlite=y kamailio5-mod-exec=y \
        kamailio5-mod-enum=y kamailio5-mod-ipops=y kamailio5-mod-dialog=y kamailio5-mod-nathelper=y kamailio5-mod-pike=y kamailio5-mod-rr=y kamailio5-mod-tm=y kamailio5-mod-usrloc=y kamailio5-mod-rtpproxy=y \
        kamailio5-mod-sipcapture=y kamailio5-mod-siptrace=y \
        kamailio5-mod-acc=y =y kamailio5-mod-alias-db=y \
        kamailio5-mod-db-flatstore=y \
        kamailio5-mod-db-text=y kamailio5-mod-dialplan=y \
        kamailio5-mod-diversion=y kamailio5-mod-domain=y kamailio5-mod-group=y \
        kamailio5-mod-htable=y kamailio5-mod-kex=y kamailio5-mod-lcr=y kamailio5-mod-maxfwd=y \
        kamailio5-mod-mediaproxy=y kamailio5-mod-mi-datagram=y kamailio5-mod-mi-fifo=y kamailio5-mod-mi-rpc=y \
        kamailio5-mod-msilo=y kamailio5-mod-nat-traversal=y kamailio5-mod-drouting=y kamailio5-mod-xmlrpc=y \
        kamailio5-mod-path=y kamailio5-mod-presence=y \
        kamailio5-mod-pv=y kamailio5-mod-qos=y kamailio5-mod-ratelimit=y kamailio5-mod-regex=y kamailio5-mod-registrar=y \
        kamailio5-mod-rls=y kamailio5-mod-rtimer=y kamailio5-mod-sanity=y \
        kamailio5-mod-siputils=y kamailio5-mod-sl=y kamailio5-mod-textops=y kamailio5-mod-tls=y \
        kamailio5-mod-tmx=y kamailio5-mod-userblacklist=y kamailio5-mod-utils=y \
        kamailio5-mod-xcap-client=y kamailio5-mod-xlog=y kamailio5-mod-xmpp=y


OWRT_ADDED_FILES += $(call AddDirFiles,honey-files)


OWRT_CONFIG_UNSET += \
	CONFIG_DEFAULT_dropbear \
	CONFIG_PACKAGE_dropbear

OWRT_CONFIG_SET += \
        BUSYBOX_CONFIG_LOGIN=y \
        BUSYBOX_CONFIG_LOGIN_SESSION_AS_CHILD=y \
        BUSYBOX_CONFIG_LOGIN_SCRIPTS=y \
        BUSYBOX_CONFIG_FEATURE_NOLOGIN=y \
        BUSYBOX_CONFIG_FEATURE_SECURETTY=y \
        BUSYBOX_CONFIG_FEATURE_EDITING_HISTORY=256 \
        BUSYBOX_CONFIG_FEATURE_EDITING_SAVEHISTORY=y \
        BUSYBOX_CUSTOM=y
