import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

public class QualityParser {

    private static String PBX = "";
    private static ArrayList<StreamLine> list = new ArrayList<StreamLine>();
    
    public static void main(String[] args) throws IOException, InterruptedException {

        // IP of the PBX
        PBX = args[0];
        
        //parse1();
        
        int parse_res = parse();
        if(parse_res == 0) {
            // Error parsing file
            System.out.println("Error parsing file!");
            return;
        }
        
        int calculate_res = merge();
        if(calculate_res == 0) {
            // Error parsing file
            System.out.println("Error merging quality!");
            return;
        }
    }
    
    private static int parse() {
        try {
            FileInputStream fis = new FileInputStream("streams.log");
            DataInputStream dis = new DataInputStream(fis);
            BufferedReader rdr = new BufferedReader(new InputStreamReader(dis));
                     
            String line = "";
            
            while((line = rdr.readLine()) != null) {
                if(line.startsWith("=") || line.startsWith("    Src") || line.startsWith("Can"));
                    // Skip those not needed lines
                else {
                    String[] parameters = line.split(" +");
                    StreamLine stream = new StreamLine();
                    
                    if(parameters[6].equalsIgnoreCase("itu-t")) {
                        for(int i = 0; i < parameters.length; i++) {
                            // Process information for further use
                            switch(i) {
                                case 1 : stream.setIp_src(parameters[i]); break;
                                case 2 : stream.setPort_src(parameters[i]); break;
                                case 3 : stream.setIp_dst(parameters[i]); break;
                                case 4 : stream.setPort_dst(parameters[i]); break;
                                case 5 : stream.setSSRC(parameters[i]); break;
                                case 6 : break;
                                case 7 : stream.setPayload(parameters[i]); break;
                                case 8 : break;
                                case 9 : break;
                                case 10 : break;
                                case 11 : stream.setLost(parameters[i]); break;
                            }
                        }
                    } else if(parameters[6].equalsIgnoreCase("gsm")){
                        for(int i = 0; i < parameters.length; i++) {
                            // Process information for further use
                            switch(i) {
                                case 1 : stream.setIp_src(parameters[i]); break;
                                case 2 : stream.setPort_src(parameters[i]); break;
                                case 3 : stream.setIp_dst(parameters[i]); break;
                                case 4 : stream.setPort_dst(parameters[i]); break;
                                case 5 : stream.setSSRC(parameters[i]); break;
                                case 6 : stream.setPayload(parameters[i]); break;
                                case 7 : break;
                                case 8 : break;
                                case 9 : break;
                                case 10 : stream.setLost(parameters[i]); break;
                            }
                        }
                    } else {
                        // Unknown codec
                    }
                    
                    // Add quality to the line, but only if PBX is dst
                    if(stream.getIp_dst().equals(PBX)) {
                        double Ie = 0.0;
                        double Bpl = 1.0;
                        
                        if(stream.getPayload().equalsIgnoreCase("g.711")) {
                            Ie = 0.0;
                        } else if(stream.getPayload().equalsIgnoreCase("gsm")) {
                            Ie = 20.0;
                        } else {
                            //No codec recognized
                        }
                        double Ieeff = Ie + (95.0 - Ie) * (stream.getLost() / (stream.getLost() + Bpl));
                        double qual_R = 94.7688 - 1.4136 - Ieeff; 
                        stream.setQualityR(qual_R);
                        
                        double qual_MOS = 1.0;
                        if(qual_R < 0.0)
                            qual_MOS = 1.0;
                        else if ((qual_R >= 0.0) && (qual_R <= 100.0))
                            qual_MOS = 1 + 0.035 * qual_R + qual_R * (qual_R - 60) * (100 - qual_R) * 7 * Math.pow(10, -6);
                        else if (qual_R > 100)
                            qual_MOS = 4.5;
                        stream.setQualityMOS(qual_MOS);
                    }
                    // Add parsed line to the list
                    list.add(stream);
                }
            }
            rdr.close();
            dis.close();
            fis.close();
            return 1;
        } catch (Exception ex) {
            // Error occured
            return 0;
        }
    }
    
    private static int merge() throws IOException, InterruptedException {
        
        Iterator iter1 = list.iterator();
        
        while(iter1.hasNext()) {
            StreamLine act1 = (StreamLine) iter1.next();
            if(act1.getIp_src().equals(PBX)) {
                Iterator iter2 = list.iterator();
                StreamLine line1 = new StreamLine();
                StreamLine line2 = new StreamLine();
                int found = 0;
                int line = 0;
                while(iter2.hasNext()){
                    StreamLine act2 = (StreamLine) iter2.next();
                    if(act2.getIp_dst().equals(PBX)) {
                        if(act2.getMerged() == false) {
                            if(act1.getSSRC().equals(act2.getSSRC())) {
                                line1 = act2;
                                act2.setMerged(true);
                                found++;
                                line = 1;
                            }
                            if(act1.getPort_src() == act2.getPort_dst()) {
                                line2 = act2;
                                act2.setMerged(true);
                                found++;
                                line = 2;
                            }
                        }
                    }
                }
                if((found == 1) && (line == 1)) {
                    Process proc = Runtime.getRuntime().exec("php-fcgi _save.php " + line1.getIp_src() + " " + line1.getIp_dst() + " " + line1.getQualityR() + " " + line1.getQualityMOS() + " " + line1.getPayload());
                    proc.waitFor();
                } else if((found == 1) && (line == 2)) {
                    Process proc = Runtime.getRuntime().exec("php-fcgi _save.php " + line2.getIp_src() + " " + line2.getIp_dst() + " " + line2.getQualityR() + " " + line2.getQualityMOS() + " " + line2.getPayload());
                    proc.waitFor();
                }                
                else if(found == 2) {
                    double new_lost = (line1.getLost() + line2.getLost()) / found;
                    double new_qual_R = (line1.getQualityR() + line2.getQualityR()) / found;
                    double new_qual_MOS = (line1.getQualityMOS() + line2.getQualityMOS()) / found;
                    StreamLine clean = new StreamLine(line1.getIp_src(), line1.getPort_src(), line2.getIp_src(), line2.getPort_src(), line1.getSSRC(), line2.getPayload()+"/"+line1.getPayload(), new_lost, new_qual_R, new_qual_MOS, true);
                   
                    Process proc = Runtime.getRuntime().exec("php-fcgi _save.php " + clean.getIp_src() + " " + clean.getIp_dst() + " " + clean.getQualityR() + " " + clean.getQualityMOS() + " " + clean.getPayload());
                    proc.waitFor();
                }
            }
        }
        // Add not merged "useful" lines laso to db
        Iterator iter3 = list.iterator();
        while(iter3.hasNext()) {
            StreamLine act = (StreamLine) iter3.next();
            if(act.getIp_dst().equals(PBX) && (act.getMerged() == false)) {
                Process proc = Runtime.getRuntime().exec("php-fcgi _save.php " + act.getIp_src() + " " + act.getIp_dst() + " " + act.getQualityR() + " " + act.getQualityMOS() + " " + act.getPayload());
                proc.waitFor();
            }
        }
        return 1;
    }

}
