
. /lib/uciprov/functions.sh

#
# logging function
#

blog() {
        logger -s -t "[Beesip]" $*
}

# Beesip environment get

beesip_env() {
        local section="directories"
        config_get BEESIP_CONFDBDIR "${section}" confdbdir /var/beesip
        config_get BEESIP_VARBDIR "${section}" vardbdir /var/beesip
        config_get BEESIP_STATDBDIR "${section}" confdbdir /var/beesip
        config_get BEESIP_STAMPDIR "${section}" confdbdir /var/lib/beesip
}

printMacAddress(){
    macAddr=$(cat /sys/class/net/$1/address)
    echo -ne $macAddr
}

printIP(){
    ip=$(ifconfig $1| grep -o "inet addr:[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"| grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*")
    echo -ne $ip
}

printNetInterfaces(){
    ifaces=$(ls /sys/class/net/)
    echo -ne $ifaces
}

printRootfsUsage(){
    echo -ne $(df -h |grep rootfs|grep -o "[0-9]*%")
}

printFreeMemory(){
    echo -ne $(free | awk '/^Mem:/{print $3}')
}

printAvailableMemory(){
    echo -ne $(free | awk '/^Mem:/{print $2}')
}

printProcessorLoad(){
    echo -ne $(uptime |awk -F'average:' '{ print $2}')
}

sysinfo(){
    echo -e "  System information as of "$(date)"\n"
    ifaces=$(printNetInterfaces)

    for i in $ifaces
    do
            echo -e "  Interface "$i":"
            echo -e "\t\t\t"$(printIP $i)
            echo -e "\t\t\t"$(printMacAddress $i)
    done

    echo -e "  Rootfs usage:\t\t"$(printRootfsUsage)
    echo -e "  Memory usage:\t\t"$(printFreeMemory) "KBytes used of" $(printAvailableMemory) "KBytes available"
    echo -e "  Processor load: \t"$(printProcessorLoad)"\n"

    if ! check_overlay;
    then
	echo -e "  Overlay not mounted!"
    fi

}

# function beesip_help
#
# returns basic parameters for a beesip command

beesip_help() {
cat <<EOF

  Beesip commandline utility.

  Usage:
    beesip help
	- This command prints help for beesip command

    beesip {factory-defaults,fd}
	- This command on squashfs filesystems restores system
	  to initial state.

    beesip patch
	- This command updates beesip to latest patchlevel for this release.

    beesip {crash-reporting,cr} {start,stop,collect}
	- This command controls crash-reporting system. If collect
	  parameter is called, it creates an archive of overlay,
	  collected logs and overlay to be used for debugging in
	  /root/ directory.

    beesip {system-upgrade,su} {path to Beesip firmware image}
	- This command upgrades system with provided firmware image.

    beesip import-gateways {stdout}
	- This command imports VoIP gateways into specified engine
	  in /etc/config/beesip. Optionally you can run it with stdout
	  parameter which only prints requested data to stdout.

    beesip setup-dialplan
	- This command prepares SIP prefixes to /etc/asterisk/sip.conf.

    beesip sysinfo
	- This command prints on stdout system information as free
	  memory, cpu usage or disk usage.

EOF
}

INTERFACE="eth0"
SSCONF="/etc/snortsam.conf"
SSLOG="/var/log/security/snortsam.log"
SNORTCFG="/etc/snort/snort.conf"
SNORTTMP="/etc/snort/snortconftemp"
SECLOG="/var/log/security"
SNORTLOG="/var/log/snort"

beesip_security(){
	case "$1" in
		start)		echo "Starting besip security."
				#pripravit kontrolu, zda ma spravne ip
				wait_for_net_addr
				WANIP=$(ifconfig $INTERFACE | grep 'inet addr:' | cut -d: -f2 | awk '{print $1}')
				echo "[besipSecurity]    IP OK"
				echo "[besipSecurity]     $WANIP"
				#1 pripravit snortsam config - smazat stary, vytvorit novy dle wanip
				if [ -f $SSCONF ]
				then
					rm -f $SSCONF
					echo "old $SSCONF deleted, creating new conf file"
				else
					echo "creating snortsam configuration file"
				fi
				echo "defaultkey besipsecurity" >> $SSCONF
				echo "accept $WANIP, besipsamagent" >> $SSCONF
				echo "logfile $SSLOG" >> $SSCONF
				echo "loglevel 3" >> $SSCONF
				echo "daemon" >> $SSCONF
				echo "iptables $INTERFACE syslog.info" >> $SSCONF

				#1.5 kontrola existence log slozek
				if [ ! -d $SECLOG ]
				then
					mkdir $SECLOG
					echo "creating $SECLOG"
				fi
				if [ ! -d $SNORTLOG ]
				then
					mkdir $SNORTLOG
					echo "creating $SNORTLOG"
				fi
				echo "Starting SnortSam"
				/etc/init.d/snortsam restart
				#3 spustit snort - tj. vygenerovat snort.conf
				echo "Starting IDS Snort"
				if [ -f $SNORTTMP ];
				then
					echo "snort temp config found"
					rm -f $SNORTTMP
				fi
				echo "recreating snort.conf"
				cp $SNORTCFG $SNORTTMP
				rm -f $SNORTCFG
				echo "var SIP_PROXY $WANIP" >> $SNORTCFG
				echo "portvar SIP_PORT 5060" >> $SNORTCFG
				tail +3 $SNORTTMP >> $SNORTCFG
				echo "starting snort deamon"
				#snort -D
				snort -i $INTERFACE -c /etc/snort/snort.conf -D
				;;
		stop)		echo "Stopping besip security. Device is now unprotected!"
				/etc/init.d/firewall stop
				killall snortsam
				killall snort.bin
				;;
	esac
}

# function crash_reporting
#
# params:
# start - starts crash reporting
# stop - stops crash reporting
# collect - creates a tgz archive

beesip_cr(){
        case "$1" in
                start)          touch /.init_enable_core
                                mkdir -p /tmp/coreDump/
                                sysctl -w "kernel.core_pattern=/tmp/coreDump/%e.%p.%s.%t.core" >> /dev/null 2>&1
                                ulimit -c unlimited
                                blog "Crash reporting has started"
                                ;;
                stop)           echo "Crash reporting has stopped"
                                echo "Not yet implemented."
                                ;;
                collect)        blog "Collecting /overlay, /var/log and /tmp/coreDump into /root/beesip-"$(uname -m)"-"$(date +%H:%M:%S-%d%m%Y)"-archive.tgz"
                                tar -czf /root/beesip-"$(uname -m)"-r"$BEESIP_REVISION"-"$(date +%H:%M:%S-%d%m%Y)"-archive.tgz /overlay/ /var/log/ /tmp/coreDump/  >> /dev/null 2>$1
                                ;;
        esac
}

beesip_include_if_ast_include_not_exist(){
	if [ -f $1 ]; then
		if !(cat $1 |grep -q $(basename $2)); then
			echo "File $(basename $2) was not included in $(basename $1). It is now."
			echo "#include \"$(basename $2)\"" >> $1
		fi
	else
		echo "File $(basename $2) was not included in $(basename $1). It is now."
		echo "#include \"$(basename 2)\"" >> $1
	fi
}

beesip_download_if_not_empty(){
	echo "Creating $(basename $1)"
	data=$(wget --no-check-certificate -q -O - $2)
	echo "Downloading from "$2

	if [ "$data" != "" ]; then
		wget --no-check-certificate -q --output-document=$1 $2
		echo -e "OK\n"
	else
		echo -e "Obtained empty data."
		echo -e "Nothing to do."
	fi
}

beesip_pbx_service(){
	config_load "beesip"

	config_get engine "gateways" import_for_engine

	if [ "$engine" == "" ]; then
		engine="ast11"
	fi

	case "$engine" in
		ast11)		echo "Restarting Asterisk"
				/etc/init.d/asterisk $1
				;;
		*)		/etc/init.d/asterisk $1
				;;
	esac

}

beesip_import_prefixes_drouting(){
	(echo "BEGIN TRANSACTION;"
	IFS=';'
	cat /etc/kamailio/prefixes.txt | while read prefix length;
	do
	  echo "delete from dr_rules where groupid='1' and prefix='$prefix';"
	  echo 'insert into dr_rules ("groupid", "prefix", "timerec", "priority", "routeid", "gwlist", "description") values '" ('1', '$prefix', '20000101T083000', '0', '0', '1', '');"
	done
	echo "COMMIT;") | sqlite3 /etc/kamailio/kamailio.db
	kamcmd drouting.reload
}

beesip_import_gateways(){
	config_load "beesip"

	config_get engine "gateways" import_for_engine
	config_get uri "gateways" import_uri

	if [ "$engine" == "" ]; then
		# default cfg engine
		engine="ast11"
	fi

	if [ "$uri" == "" ]; then
		# if uri is not set, then set default uri to gateway database export
		uri=https://iptelix.cesnet.cz/SIP-routing
	fi

        # Handle export for each voip engine
	case "$engine" in

	 ast11)
		echo "Importing gateways.."

		beesip_download_if_not_empty /etc/asterisk/beesip-sip-gateways.conf $uri"/exportAsterisk/11/Sip.conf"
		beesip_download_if_not_empty /etc/asterisk/beesip-extensions-gateways.conf $uri"/exportAsterisk/11/Extensions.conf"

		beesip_include_if_ast_include_not_exist /etc/asterisk/sip.conf /etc/asterisk/beesip-sip-gateways.conf
		beesip_include_if_ast_include_not_exist /etc/asterisk/extensions.conf /etc/asterisk/beesip-extensions.conf

		beesip_pbx_service restart
		;;
	kamailio)
		beesip_download_if_not_empty /etc/kamailio/prefixes.txt $uri"/../prefixExport"
		beesip_import_prefixes_drouting
		;;
	*)
		blog "Not implemented yet. This should be default for any asterisk engine."
		;;
	esac
}

beesip_setup_dialplan(){
	config_load "beesip"
	config_get engine "gateways" import_for_engine

	if [ "$engine" == "" ]; then
		engine="ast11"
	fi

	if [ "$engine" == "ast11" ]; then
		echo "Preparing configuration.."
		cp /etc/beesip/astdefaults/beesip-extensions.conf /etc/asterisk/beesip-extensions.conf

		beesip_include_if_ast_include_not_exist /etc/asterisk/extensions.conf /etc/asterisk/beesip-extensions.conf

		echo "Preparing prefixes.."

		config_get countrycode "gateways" country_code
		config_get prefix "gateways" prefix

		sed -i "s/E164PREFIX/$countrycode$prefix/g" /etc/asterisk/beesip-extensions.conf
		sed -i "s/LOCALPREFIX/$prefix/g" /etc/asterisk/beesip-extensions.conf
		sed -i "s/COUNTRYCODE/$countrycode/g" /etc/asterisk/beesip-extensions.conf

		beesip_pbx_service restart
	fi
}

beesip_factorydefaults() {
        local cmddev=$1
        local dev
        
	if kernel_getflag overlay;
	then
		ovr=$(kernel_getvar overlay)
		if echo $ovr | grep -q ',' && [ -z "$cmddev" ]; then
		  echo "There are more choices ($ovr). You must select right device to erase."
		  echo "beesip factory-defaults device"
		  return 1
		else
		  if echo "$ovr" | grep -q "$cmddev" && [ -b "$cmddev" ]; then
		    (echo 'BeeSipData'; dd if=/dev/zero bs=1M count=1) >$cmddev && reboot -f
		  else
		    echo "Error! Given device ($cmddev) is not listed in kernel overlay option ($ovr) or is not block device! Exiting."
		    return 1
		  fi
		fi
	else
		yes | head -3 | jffs2reset && reboot -f
	fi
}

beesip_sysupgrade() {
	if [ -z "$1" ]; then
		config_load beesip
		config_get base_uri "global" base_uri
		config_get sysupgrade_img "global" sysupgrade_img
		if [ -n "$base_uri" ] && [ -n "$sysupgrade_img" ]; then
			cd /tmp && curl --capath /etc/ssl/certs -f -s -L -o "${sysupgrade_img}" "${base_uri}/${sysupgrade_img}" && syupgrade ${sysupgrade_img}
		else
			echo "ERROR: Please, provide path to image file in config file or enter url."
		fi
	else
		if [ -f "$1" ]; then
			sysupgrade -v "$1"
		else
		  if [ -n "$1" ]; then
			curl --capath /etc/ssl/certs -f -s -L -o /tmp/sysupgrade.img "$1"
			sysupgrade -v /tmp/sysupgrade.img
		  else
			echo "ERROR: Upgrade Beesip image at $1 not found."
		  fi
		fi
	fi
}

beesip_prepare_kamailiodb() {
  ! [ -f /etc/kamailio/kamctlrc ] && return 1
  . /etc/kamailio/kamctlrc 
  (echo y; echo y) |kamdbctl create
	sqlite3 $DB_PATH <<EOF
  ALTER TABLE acc ADD COLUMN src_user VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN src_domain VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN dst_ouser VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN dst_user VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN dst_domain VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE acc ADD COLUMN src_ip VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN src_user VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN src_domain VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN dst_ouser VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN dst_user VARCHAR(64) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN dst_domain VARCHAR(128) NOT NULL DEFAULT '';
  ALTER TABLE missed_calls ADD COLUMN src_ip VARCHAR(128) NOT NULL DEFAULT '';
EOF
  [ -n "$SIPTRACEDB" ] && sqlite3 $SIPTRACEDB <<EOF
  CREATE TABLE sip_trace (
    id INTEGER PRIMARY KEY NOT NULL,
    time_stamp TIMESTAMP WITHOUT TIME ZONE DEFAULT '1900-01-01 00:00:01' NOT NULL,
    time_us INTEGER DEFAULT 0 NOT NULL,
    callid VARCHAR(255) DEFAULT '' NOT NULL,
    traced_user VARCHAR(128) DEFAULT '' NOT NULL,
    msg TEXT NOT NULL,
    method VARCHAR(50) DEFAULT '' NOT NULL,
    status VARCHAR(128) DEFAULT '' NOT NULL,
    fromip VARCHAR(50) DEFAULT '' NOT NULL,
    toip VARCHAR(50) DEFAULT '' NOT NULL,
    fromtag VARCHAR(64) DEFAULT '' NOT NULL,
    direction VARCHAR(4) DEFAULT '' NOT NULL
)
EOF
}

#
# Prepare BeeSIP after clean factory defaults
#

beesip_prepare() {
	blog "Preparing Beesip ..."
	ssl_rehash
	beesip_import_gateways
	beesip_prepare_kamailiodb
}

beesip_init() {
        local enabled
        local prepared
	local crashreport
	config_load beesip
        config_get_bool enabled "global" enable 1
	config_get_bool prepared "global" prepared 0
	config_get_bool crashreport "global" crashreport 0

        [ "${enabled}" -eq 0 ] && { echo "Beesip disabled"; return 1; }
        ([ "${prepared}" -eq 0 ] && beesip_prepare && uci set beesip.beesip.prepared=1 && uci commit) &
	if [ "${crashreport}" -eq 1 ];
	then
		beesip_cr start
		echo "Beesip: starting crash reporting"
	fi
	blog "Great init of Beesip"
}
