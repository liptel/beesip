#!/bin/bash

echo "***************************************"
echo "Creating repo"
echo "***************************************"

MKREPO=$(readlink -f $0)
IPKG_INDEX=$(dirname $MKREPO)/ipkg-make-index.sh

for oversion in owrt_*; do
  echo -n "Scanning packages for $oversion: "
  [ -d $oversion ] || continue
  pushd $oversion >/dev/null
  for obin in *; do
    [ -d $obin/packages ] || continue
    echo -n "$obin "
    pushd $obin/packages >/dev/null
    $IPKG_INDEX . >Packages
    cat Packages | gzip >Packages.gz
    popd >/dev/null
  done
  popd >/dev/null
  echo
done
echo

FILETYPES="img vmdk iso squashfs vmlinuz sysupgrade Packages Packages.gz"
echo "Computing MD5SUM for following file types: "$FILETYPES": "
echo

for i in $FILETYPES;
do
	FILES=$(find . -name "*$i")
	for j in $FILES;
	do
		filename=$(basename "$j")
                extension="${filename##*.}"
                file="${filename%.*}"

		echo "$j "

		FULL_MD5SUM=$(md5sum $j)
		MD5SUM=${FULL_MD5SUM% *}

		echo $MD5SUM > $j".md5sum"
		echo $MD5SUM $filename >> $(dirname $j)/md5sums
	done
done

echo

