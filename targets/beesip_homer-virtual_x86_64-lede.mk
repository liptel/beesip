$(eval $(call BeesipDefaults,x86_64))
$(eval $(call BeesipDefaults,virtual))
$(eval $(call BeesipDefaults,homer))

TARGET_NAME=homer_virtual-$(BEESIP_VERSION)-$(TARGET_CPU)

BEESIP_PACKAGES+=
BEESIP_NAME=trunk

LEDE=1
OWRT_NAME=trunk
OWRT_IMG_PROFILE=Generic
OWRT_CONFIG_SET+=TARGET_KERNEL_PARTSIZE=30 TARGET_ROOTFS_PARTSIZE=300
