TARGET_CPU=mvebu
OWRT_NAME=chaos_calmer
BEESIP_NAME=chaos_calmer
OWRT_GIT=https://gitlab.labs.nic.cz/turris/openwrt.git

TARGET_NAME=eduroomap_$(HWREV)_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=arm
TARGET_QEMU_OPTS=-m64

$(eval $(call BeesipDefaults,eduroam))
$(eval $(call BeesipDefaults,eduroom))

OWRT_IMG_BIN_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-factory.bin
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-vmlinux.elf
OWRT_IMG_PROFILE=omnia

OWRT_CONFIG_SET +=  TARGET_mvebu=y TARGET_mvebu_Turris-Omnia=y TARGET_BOARD="mvebu" \
  TARGET_ARCH_PACKAGES="mvebu" \
  CPU_TYPE="cortex-a9+vfpv3" LINUX_4_4=y DEFAULT_ath10k-firmware-qca988x=y 

OWRT_CONFIG_UNSET += SIGNED_PACKAGES

BEESIP_PACKAGES += cups=n libavahi-client=n splix=n libcupscgi=n kmod-ipv6=n luci=y






