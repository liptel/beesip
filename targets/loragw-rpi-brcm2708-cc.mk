TARGET_CPU=brcm2708
OWRT_NAME=chaos_calmer
BEESIP_NAME=chaos_calmer

BEESIP_NODEFAULTS=files uciprov

TARGET_NAME=loragw_$(HWREV)_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)

$(eval $(call AddDefaultOpenWrtFeed,src-git misc https://github.com/limosek/openwrt-misc.git))
$(eval $(call BeesipDefaults,loragw))

OWRT_IMG_BIN_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-$(HWREV)-squashfs-factory.bin
OWRT_IMG_PROFILE=ARCHERC7
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-vmlinux.elf

OWRT_CONFIG_SET +=  TARGET_brcm2708=y TARGET_brcm2708_bcm2708=y TARGET_brcm2708_bcm2708_RaspberryPi=y



