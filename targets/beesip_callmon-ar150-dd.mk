TARGET_CPU=ar71xx
OWRT_NAME=designated_driver
BEESIP_NAME=designated_driver

TARGET_NAME=callmon_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

$(eval $(call BeesipDefaults,callmon))

BEESIP_PACKAGES += mc=n asterisk11-sounds=n aterisk11-chan-ooh323=n asterisk11-voicemail=n openssh-client-utils=n asterisk11-chan-iax2=n ppp=n tc=n 

OWRT_IMG_BIN_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-factory.bin
OWRT_IMG_PROFILE=GL-AR150
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-vmlinux.elf
IB_NAME=Beesip-ImageBuilder-$(BEESIP_VERSION)-$(TARGET_CPU)-$(SUBTARGET).$(PKG_OS)-$(PKG_CPU)
#/opt/build/beesip/build/owrt-ar71xx-designated_driver/bin/ar71xx/OpenWrt-ImageBuilder-1.4.1-ar71xx-generic.Linux-x86_64.tar.bz2: Cannot open: No such file or directory


OWRT_CONFIG_SET += TARGET_ar71xx=y TARGET_ar71xx_generic_GL-AR150=y IB=y
