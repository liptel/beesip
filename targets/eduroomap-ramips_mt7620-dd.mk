TARGET_CPU=ramips-mt7620
OWRT_NAME=designated_driver
BEESIP_NAME=designated_driver

TARGET_NAME=eduroomap_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

$(eval $(call BeesipDefaults,eduroam))
$(eval $(call BeesipDefaults,eduroom))

BEESIP_PACKAGES += kmod-ipv6=n

OWRT_IMG_BIN_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-factory.bin
#OWRT_IMG_PROFILE=GL-AR150
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-vmlinux.elf
IB_NAME=Beesip-ImageBuilder-$(BEESIP_VERSION)-$(TARGET_CPU)-$(SUBTARGET).$(PKG_OS)-$(PKG_CPU)


OWRT_CONFIG_SET += TARGET_ramips=y TARGET_ramips_mt7620=y TARGET_ramips_mt7620_Default=y

