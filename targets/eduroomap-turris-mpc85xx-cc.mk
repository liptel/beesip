
TARGET_CPU=mpc85xx
OWRT_NAME=chaos_calmer
BEESIP_NAME=chaos_calmer
OWRT_GIT=https://gitlab.labs.nic.cz/turris/openwrt.git

TARGET_NAME=eduroomap_$(HWREV)_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)

$(eval $(call BeesipDefaults,eduroam))
$(eval $(call BeesipDefaults,eduroom))

OWRT_IMG_BIN_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(BEESIP_VERSION)-ar71xx-generic-tl-$(HWREV)-squashfs-factory.bin
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-generic-vmlinux.elf

OWRT_CONFIG_SET += TARGET_mpc85xx=y TARGET_mpc85xx_p2020_nand=y TARGET_mpc85xx_p2020_nand_TURRISNAND=y HAS_SUBTARGETS=y TARGET_BOARD=mpc85xx

OWRT_CONFIG_UNSET += SIGNED_PACKAGES

BEESIP_PACKAGES += cups=n libavahi-client=n splix=n libcupscgi=n kmod-ipv6=n luci=y






