TARGET_CPU=ramips
SUBTARGET=mt7620
OWRT_NAME=chaos_calmer
BEESIP_NAME=chaos_calmer

TARGET_NAME=eduroomap_$(HWREV)_$(BEESIP_VERSION)-owrt_$(OWRT_NAME)
HWREV=wt3020-8M

TARGET_QEMU=mips
TARGET_QEMU_OPTS=-m64

$(eval $(call BeesipDefaults,eduroam))
$(eval $(call BeesipDefaults,eduroom))

OWRT_IMG_BIN_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-$(SUBTARGET)-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_SYSUPGRADE_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-$(SUBTARGET)-$(HWREV)-squashfs-sysupgrade.bin
OWRT_IMG_FACTORY_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-$(SUBTARGET)-$(HWREV)-squashfs-factory.bin
OWRT_IMG_PROFILE=WT3020-only
OWRT_IMG_KERNEL_NAME=openwrt-$(BEESIP_VERSION)-$(TARGET_CPU)-$(SUBTARGET)-vmlinux.elf

OWRT_CONFIG_SET +=  TARGET_ramips=y TARGET_ramips_mt7620=y TARGET_ramips_mt7620_WT3020-only=y
OWRT_PATCHES += ramips-add-new-profile-for-WT3020-only.patch

