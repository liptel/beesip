#
# Copyright (C) 2013 OpenWrt
#
# This is free software, licensed under the GNU General Public License v2.
# See /LICENSE for more information.
#

include $(TOPDIR)/rules.mk

PKG_NAME:=openbts
PKG_SOURCE_VERSION:=5004
PKG_VERSION:=r$(PKG_SOURCE_VERSION)
PKG_RELEASE:=1

PKG_SOURCE_PROTO:=svn
PKG_SOURCE_URL:=http://wush.net/svn/range/software/public/
PKG_SOURCE:=$(PKG_NAME)-$(PKG_VERSION).tar.gz
PKG_SOURCE_SUBDIR:=$(PKG_NAME)-$(PKG_VERSION)
PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)

CONFIGURE_VARS += 

include $(INCLUDE_DIR)/package.mk

MAKE_FLAGS += CPPFLAGS="-L$(STAGING_DIR)/usr/lib" CFLAGS="-I$(STAGING_DIR)/usr/include"

define Package/openbts
  SECTION:=net
  CATEGORY:=Network
  SUBMENU:=Telephony
  TITLE:=OpenBTS
  URL:=http://openbts.sourceforge.net/
  DEPENDS:=+libosip2 +libsqlite3 +libortp +libusb-1.0 +libstdcpp +sqlite3-cli
endef

define Package/smqueue
  SECTION:=net
  CATEGORY:=Network
  SUBMENU:=Telephony
  TITLE:=SMS queue for OpenBTS
  URL:=http://openbts.sourceforge.net/
  DEPENDS:=+openbts
endef

define Package/subscriberregistry
  SECTION:=net
  CATEGORY:=Network
  SUBMENU:=Telephony
  TITLE:=Subscriber registry for OpenBTS
  URL:=http://openbts.sourceforge.net/
  DEPENDS:=+openbts
endef

define Package/openbts/description
OpenBTS is a Unix application that uses a software radio to
present a GSM air interface to standard 2G GSM handset and uses
a SIP softswitch or PBX to connect calls.
endef

define Package/smqueue/description
Smqueue is an RFC-3428 store and forward server, used to support SMS
in OpenBTS installations. Smqueue runs alongside Asterisk to provide
SMS routing service.
endef

define Package/subscriberregistry/description
Subscriber registry.
endef

OPENBTS_CONFIGURE_ARGS = \
	--with-resamp

# dodat --with-uhd po dodani balicku uhd

define Build/Configure
	cd $(PKG_BUILD_DIR)/openbts/trunk; autoreconf -i
	$(call Build/Configure/Default,$(OPENBTS_CONFIGURE_ARGS),,/openbts/trunk)
	cd $(PKG_BUILD_DIR)/smqueue/trunk; autoreconf -i
	$(call Build/Configure/Default,,,/smqueue/trunk)
endef

define Build/Compile
	$(MAKE) -C $(PKG_BUILD_DIR)/openbts/trunk $(MAKE_FLAGS)
	$(MAKE) -C $(PKG_BUILD_DIR)/smqueue/trunk $(MAKE_FLAGS)
	$(MAKE) -C $(PKG_BUILD_DIR)/subscriberRegistry/trunk $(MAKE_FLAGS) 
endef

define Package/openbts/install
	$(INSTALL_DIR) $(1)/usr/sbin
	$(CP) $(PKG_BUILD_DIR)/openbts/trunk/apps/OpenBTS $(1)/usr/sbin/
	$(INSTALL_DIR) $(1)/etc/OpenBTS
	$(CP) \
		$(PKG_BUILD_DIR)/openbts/trunk/apps/OpenBTS.example.sql \
		$(1)/etc/OpenBTS/
endef

define Package/smqueue/install
	$(INSTALL_DIR) $(1)/usr/sbin
	$(CP) $(PKG_BUILD_DIR)/smqueue/trunk/smqueue/smqueue $(1)/usr/sbin/
	$(INSTALL_DIR) $(1)/etc/OpenBTS
	$(CP) \
		$(PKG_BUILD_DIR)/smqueue/trunk/smqueue/smqueue.example.sql \
		$(1)/etc/OpenBTS/
endef

define Package/subscriberregistry/install
	$(INSTALL_DIR) $(1)/usr/sbin
	$(CP) \
		$(PKG_BUILD_DIR)/subscriberRegistry/trunk/sipauthserve \
		$(1)/usr/sbin/
endef

$(eval $(call BuildPackage,openbts))
$(eval $(call BuildPackage,smqueue))
$(eval $(call BuildPackage,subscriberregistry))
