/* 
 * File:   ConfigurationParameters.cpp
 * Author: Erik Grešák
 * 
 * Created on January 6, 2017, 12:36 PM
 */

#include <iosfwd>
#include <string>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <map>

#include "ConfigurationParameters.h"

using namespace std;

ConfigurationParameters::ConfigurationParameters() {
    this->initialization();
}

ConfigurationParameters::ConfigurationParameters(const ConfigurationParameters& orig) {
}

ConfigurationParameters::~ConfigurationParameters() {
}

void ConfigurationParameters::selectStringForDoubleValue(string str, vector<string> pre) {

    for (int i = 0; i < pre.size(); i++) {
        int size = pre[i].size();
        if (str.compare(0, size, pre[i]) == 0) {
            string token = str.substr(size);
            ConfigurationParameters::doubleMap[pre[i]] = atof(token.c_str());
        };
    }

}

void ConfigurationParameters::selectStringForUint32Value(string str, vector<string> pre) {

    for (int i = 0; i < pre.size(); i++) {
        int size = pre[i].size();
        if (str.compare(0, size, pre[i]) == 0) {
            string token = str.substr(size);
            ConfigurationParameters::uint32Map[pre[i]] = atoi(token.c_str());
        };
    }

}

void ConfigurationParameters::selectStringForStringValue(string str, vector<string> pre) {

    for (int i = 0; i < pre.size(); i++) {
        int size = pre[i].size();
        if (str.compare(0, size, pre[i]) == 0) {
            string token = str.substr(size);
            token.erase(0,1);
            ConfigurationParameters::stringMap[pre[i]] = token;
        };
    }

}

bool ConfigurationParameters::isComment(string str){
    char hes = '#';
    const char * c = str.c_str();

    if (hes == c[0]) {
        return true;
    }
    return false;
}

void ConfigurationParameters::setEnable(string str){
    string tg = "ms enable";
    
    if (str.compare(0, tg.size(), tg) == 0){
        ConfigurationParameters::ms = true;
    }
}

void ConfigurationParameters::readConfig(){
    ifstream file("/etc/loracol/loracol.conf");
    string str;
    ConfigurationParameters::ms = false;

    while (getline(file, str)) {
        if (!this->isComment(str)) {
            this->runSetting(str);
        }
    }

}

void ConfigurationParameters::initialization() {

    // All names parameters
    ConfigurationParameters::nameParameters.push_back("set_center_frequency");
    ConfigurationParameters::nameParameters.push_back("set_location_lat");
    ConfigurationParameters::nameParameters.push_back("set_location_long");
    ConfigurationParameters::nameParameters.push_back("set_location_alt");
    ConfigurationParameters::nameParameters.push_back("email");
    ConfigurationParameters::nameParameters.push_back("description");
    ConfigurationParameters::nameParameters.push_back("ms_address");
    ConfigurationParameters::nameParameters.push_back("ms_client_id");
    ConfigurationParameters::nameParameters.push_back("ms_port");
    ConfigurationParameters::nameParameters.push_back("ms_password");
    ConfigurationParameters::nameParameters.push_back("ms_username");
    ConfigurationParameters::nameParameters.push_back("ms_qos");
    ConfigurationParameters::nameParameters.push_back("ms_timeout");
    ConfigurationParameters::nameParameters.push_back("app_euis");

    // Initialization double vector
    ConfigurationParameters::doubleMap["set_location_lat"] = 0.0;
    ConfigurationParameters::doubleMap["set_location_long"] = 0.0;
    ConfigurationParameters::doubleMap["set_location_alt"] = 0.0;
    
    // Initialization uint32 vector
    ConfigurationParameters::uint32Map["set_center_frequency"] = 0;

    // Initialization string vector
    ConfigurationParameters::stringMap["email"] = "null";
    ConfigurationParameters::stringMap["description"] = "null";
    ConfigurationParameters::stringMap["ms_client_id"] = "null";
    ConfigurationParameters::stringMap["ms_address"] = "null";
    ConfigurationParameters::stringMap["ms_password"] = "null";
    ConfigurationParameters::stringMap["ms_username"] = "null";
    ConfigurationParameters::stringMap["app_euis"] = "null";
    
}

void ConfigurationParameters::runSetting(string str) {
    this->setEnable(str);
    this->selectStringForUint32Value(str, this->nameParameters);
    this->selectStringForDoubleValue(str, this->nameParameters);
    this->selectStringForStringValue(str, this->nameParameters);
    this->setField();

}

void ConfigurationParameters::setField() {

    ConfigurationParameters::location_lat = this->doubleMap["set_location_lat"];
    ConfigurationParameters::location_long = this->doubleMap["set_location_long"];
    ConfigurationParameters::location_alt = this->doubleMap["set_location_alt"];
    ConfigurationParameters::center_frequency = this->uint32Map["set_center_frequency"];
    ConfigurationParameters::ms_address = this->stringMap["ms_address"];
    ConfigurationParameters::ms_password = this->stringMap["ms_password"];
    ConfigurationParameters::ms_username = this->stringMap["ms_username"];
    ConfigurationParameters::ms_client_id = this->stringMap["ms_client_id"];
    ConfigurationParameters::email = this->stringMap["email"];
    ConfigurationParameters::description = this->stringMap["description"];
    ConfigurationParameters::app_euis = this->stringMap["app_euis"];

}