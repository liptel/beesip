/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ConfigurationParameters.h
 * Author: root
 *
 * Created on January 6, 2017, 12:36 PM
 */

#include <string>
#include <vector>
#include <stdint.h>
#include <map>

#ifndef CONFIGURATIONPARAMETERS_H
#define CONFIGURATIONPARAMETERS_H

using namespace std;

class ConfigurationParameters {
public:
    double location_lat;
    double location_long;
    double location_alt;
    uint32_t center_frequency;
    string ms_address;
    string ms_password;
    string ms_username;
    string ms_client_id;
    string email;
    string description;
    string app_euis;
    bool ms;
    ConfigurationParameters();
    ConfigurationParameters(const ConfigurationParameters& orig);
    virtual ~ConfigurationParameters();
    void readConfig();

private:
    vector<string> nameParameters;
    map<string, double> doubleMap;
    map<string, string> stringMap;
    map<string, uint32_t> uint32Map;
    void selectStringForDoubleValue(string str, vector<string> pre);
    void selectStringForStringValue(string str, vector<string> pre);
    void selectStringForUint32Value(string str, vector<string> pre);
    void runSetting(string str);
    void initialization();
    void setEnable(string str);
    void setField();
    bool isComment(string str);


};

#endif /* CONFIGURATIONPARAMETERS_H */

