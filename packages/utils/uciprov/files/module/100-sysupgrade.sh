#!/bin/sh

uciprov_initmodule2 sysupgrade sysupgrade

# if $1 is given, force to sysupgrade
uciprov_sysupgrade(){
	local uri=$(uciprov_runmodule2 "$@") || return 1;
	local suimg="$UCITMP/su$$.img"
	local save
	
	if [ -n "$uri" ];
	then
		if [ "$factorydefault" = "1" ]; then
		  tar -czf $UCITMP/su.tgz /etc/config/uciprov /etc/dropbear/
		  save="-f $UCITMP/su.tgz"
		else
		  save=""
		fi
		mount -o remount,size=5G /tmp/ # Increase tmpfs size.
		fetch_uri "$uri" >$suimg && \
		md5=$(md5sum $suimg | cut -d ' ' -f 1) && \
		uciprov_donemodule2 sysupgrade "$md5" && \
		cd / && sysupgrade $save $suimg || mblog sysupgrade "Error during sysupgrade $save $suimg!"
	fi
}

sysupgrade_help(){
	echo "Uciprov sysupgrade module"
}


