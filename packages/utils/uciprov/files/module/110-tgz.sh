#!/bin/sh

uciprov_initmodule2 tgz tar

uciprov_tgz(){
	local uri=$(uciprov_runmodule2 "$@") || return 1
	local tgzf="$UCITMP/uciprov_tgz$$.tgz"
	if [ -n "$uri" ];
	then
		cd /
		fetch_uri "$uri" >$tgzf && \
		tar -zxvf $tgzf && \
		ssl_rehash && \
		uciprov_donemodule2 tgz $(md5sum $tgzf | cut -d ' ' -f 1) || \
		mblog tgz "Error untaring tgz!"
	fi
}

uci_tgz_help(){
	echo "Uciprov tar.gz files deployment module"
}

