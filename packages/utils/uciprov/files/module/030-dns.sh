#!/bin/sh

if uciprov_initmodule1 dnssec unbound-host; then
	uciprov_hook_add uciprov_geturi get_uri_by_dnssec
fi

if uciprov_initmodule1 dns unbound-host; then
	uciprov_hook_add uciprov_geturi get_uri_by_dns
fi

get_uri_by_dns(){

	config_load uciprov
	config_get dns_prefix "global" dns_prefix
	[ -z "$dns_prefix" ] && return 

	local dnsc
	local hostbin
	if  which unbound-host >/dev/null;
	then
		hostbin=$(which unbound-host)
		dnsc=unbound
	else
		if  which host >/dev/null;
		then
			hostbin=$(which host)
			dnsc=bind
		fi
	fi

	if [ -x "$hostbin" ];
	then
		if [ -n "$secure" ];
		then
			[ "$dnsc" = "bind" ] && { blog "Cannot use DNSSEC, unbound-host not found!" ; return; }
			! [ -f /etc/unbound/root.key ] && { blog "Cannot use DNSSEC, /etc/unbound/root.key not found!"; return; }
			secopts="-v -f /etc/unbound/root.key"
		fi
		local opts=""
			
		if [ -z "$secure" ];
		then
			cmd="$hostbin $secopts $opts -t txt $dns_prefix |grep -v 'not found:'"
		else
			cmd="$hostbin $secopts $opts -t txt $dns_prefix |grep -v 'not found:' |grep '(secure)\$'"
		fi
		eval $cmd |while read foo foo foo foo txt
		do
			echo setmacro uci_uris $txt
			echo setmacro dns_uri $txt
			blog "Got uri from DNS (secure=$secure,$txt)"
		done >/tmp/uciprov_dns$$.sh
		. /tmp/uciprov_dns$$.sh
		rm -f /tmp/uciprov_dns$$.sh
	fi
}

get_uri_by_dnssec(){
	secure=1
	get_uri_by_dns
}

dns_help(){
      echo "DNS stage1 module"
}
dnssec_help(){
      echo "DNSSEC stage1 module"
}

