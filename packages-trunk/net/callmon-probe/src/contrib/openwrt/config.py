#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pyuci.pyuci import uci

class Config(object):
    host                        = uci.get("callmon","global","host")
    port                        = uci.get("callmon","global","port")
    zabbix_login                = uci.get("callmon","global","zabbix_login")
    zabbix_password             = uci.get("callmon","global","zabbix_password")
    zabbix_url                  = 'http://%s/zabbix'                        % host
    server_ssh_login_username   = uci.get("callmon","global","server_ssh_login_username")
    tree_base                   = uci.get("callmon","global","tree_base")
    base_dir                    = '%s/python/'                              % tree_base
    bckup_wav_dir               = '%s/python/archive/'                      % tree_base
    log_base                    = '%s/python/logs/'                         % tree_base
    template_sipconf            = '%s/templates/asterisk/sip.conf'          % tree_base
    sipconf                     = '%s/asterisk/sip.conf'                    % tree_base
    template_extconf            = '%s/templates/asterisk/extensions.conf'   % tree_base
    extconf                     = '%s/asterisk/extensions.conf'             % tree_base
    rsa_key_path                = '%s/ssh/sonda1_rsa'                       % tree_base
    reference_wav               = '%s/wavs/test_ulaw_25s.wav'               % tree_base
    recorded_dir                = uci.get("callmon","global","recorded_dir")
    probe_cron_user             = uci.get("callmon","global","probe_cron_user")
    asterisk_ami_user           = uci.get("callmon","global","asterisk_ami_user")
    asterisk_ami_secret         = uci.get("callmon","global","asterisk_ami_secret")
    probe_com_interface_name    = uci.get("callmon","global","probe_com_interface_name")
    astsipconf                  = uci.get("callmon","global","astsipconf")
    server_wav_dir              = uci.get("callmon","global","server_wav_dir")


