Callmon on OpenWrt
------------------

Callmon on OpenWrt needs pyuci to be used. You can find it
on the link below.

https://bitbucket.org/liptel/pyuci

Contents
--------

  - config.py represents callmon configuration that uses pyuci to get the values from UCI config file
  - callmon.config represents UCI configuration that should be copied to /etc/config location without ".config" postfix in filename