#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
from callmon.libs.controller_class import Controller
from callmon.config import Config
from pprint import pprint
from random import randint


def clear_cron():
    """
    Function to disable all active cron tasks
    """
    Con.remove_all_crons()
    return

def create_or_activate_probe():
    """
    Checks if probe exists on the server. If it does, then it activates it and creates
    appropriate crontab entries.
    """
    # Setting minute in hour when scripts should be run
    minuteInHourCall    = randint(0,29)               # random minute in first half hour
    minuteInHourAnalyze = minuteInHourCall + 30       # analyze after 30 minutes

    if Con.probe_exist():                                       # can be true or false
        if Con.activate_probe():                                # can be true or false
            print "Status of probe was changed to active on zabbix server"
            #Generate sip config
            Con.generate_conf()
            print "Configuration files successfully generated"

            Con.remove_all_crons()
            print "Old crons removed"

            #Activate cron for sending calls
            Con.add_cron_in_hour('callmonp make_call >> %slogs/calls.log' % (Cfg.base_dir), minuteInHourCall)
            #Activate cron for analyzing result wavs
            Con.add_cron_in_hour('callmonp analyze >> %slogs/analyze.log' % (Cfg.base_dir), minuteInHourAnalyze)
            print "Actual cron tasks:"
            Con.print_cron()
        else:
            print "Error: Activation of probe cannot be done, probe not found"
    else:
        Con.create_probe()
        print "New probe created on zabbix server"
        create_or_activate_probe()
    return

def call_maker():
    print "Start generating calls..."
    Con.send_calls()
    return

def analyzer():
    Con   = Controller()
    files = Con.rsync()                 # send new files to server
    for f in files:
        print f + ' sent to server'
        Con.run_analyzation(f)          # add every file to analyzation queue
    return

def create_edges():
    """
    This function updates edges on all probes, i.e. when new probe is added to the network,
    all other probes are notified and updated.
    """
    print "Updating edges..."
    Con.create_edges()
    return

def extensions():
    with open(Cfg.template_extconf, 'r') as f:
        content = f.read()

    with open(Cfg.extconf, 'w') as f:
        f.write(content.replace('##FILETOPLAY##', Cfg.reference_wav.rstrip('.wav')))

    Con.run_cmd('asterisk', args=['-rx', 'core restart now'])
    return

def refresh_conf():
    Con.generate_conf()
    return


if __name__ == "__main__":
    possible_args = {
        'analyze'               : analyzer,
        'clear_cron'            : clear_cron,
        'create_edges'          : create_edges,
        'create_extensions'     : extensions,
        'main'                  : create_or_activate_probe,
        'make_call'             : call_maker,
        'refresh_configuration' : refresh_conf,
        }

    Cfg = Config()
    Con = Controller(Cfg)
    try:
        action = sys.argv[1]
    except:
        print 'No action given, please use one of: {%s}' % '|'.join(possible_args.keys())
        sys.exit()
    if action in possible_args.keys():
        possible_args[action]()
    else:
        print 'Invalid action given, please use one of: {%s}' % '|'.join(possible_args.keys())




