#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Config(object):
    ## Communication configuration
    # Zabbix sever IP address
    host                        = 'ZABBIXIP'
    # Zabbix port to communicate
    port                        = ZABBIXPORT
    # Zabbix server web admin username
    zabbix_login                = 'ZABBIXLOGIN'
    # Zabbix server web admin password
    zabbix_password             = 'ZABBIXPASSWORD'
    # Zabbix fronted web interface
    zabbix_url                  = 'http://%s/zabbix'                        % host
    # Username the probe will use to log in to the zabbix server using SSH
    server_ssh_login_username   = 'ZABBIXSSHUSER'

    ## Probe files and directories configuration
    # Main directory, under which all the scripts are placed
    tree_base                   = 'PROBEBASEDIR'
    # Python base directory
    base_dir                    = '%s/python/'                              % tree_base
    # backup directory for the wav files before they are sent to server
    bckup_wav_dir               = '%s/python/archive/'                      % tree_base
    # directory for the log files
    log_base                    = '%s/python/logs/'                         % tree_base
    # template for sip.conf
    template_sipconf            = '%s/templates/asterisk/sip.conf'          % tree_base
    # generated sip.conf
    sipconf                     = '%s/asterisk/sip.conf'                    % tree_base
    # template for extensions.conf
    template_extconf            = '%s/templates/asterisk/extensions.conf'   % tree_base
    # generated extensions.conf
    extconf                     = '%s/asterisk/extensions.conf'             % tree_base
    # SSH private key
    rsa_key_path                = '%s/ssh/sonda1_rsa'                       % tree_base
    # Reference wav file to be played in calls
    reference_wav               = '%s/wavs/test_ulaw_25s.wav'               % tree_base
    # Asterisk monitor directory
    recorded_dir                = 'ASTMONDIR'
    # Cron user for the task autostart
    probe_cron_user             = 'PROBECRONUSER'
    # Asterisk AMI interface username
    asterisk_ami_user           = 'PROBEASTAMIUSER'
    # Asterisk AMI interface password
    asterisk_ami_secret         = 'PROBEASTAMIPASSWORD'
    # Probe interface used for all the communications
    probe_com_interface_name    = 'PROBEINTERFACE'
    # Asterisk configuration for sip channel
    astsipconf                  = 'PROBEASTSIPCONF'

    ## Server files and directories configuration
    # Directory where the wav files are stored on the server
    server_wav_dir              = '/home/python/data_wavs'


